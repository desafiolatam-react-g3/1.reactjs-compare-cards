EJERCICIO 1: COMPARAR TARJETAS
---
Se debe desarrollar una aplicación que a partir de una fuente de datos, genere una lista de productos. Siendo cada producto una tarjeta.

Al seleccionar una tarjeta, se debe añadir el detalle del producto a un listado de comparación. Al seleccionar por segunda vez la misma tarjeta, se debe remover el producto del listado de comparación.

El listado de comparación debe mostrar el precio, colores disponibles y condición de cada producto añadido.  

### Fuente de datos
- Productos: `./public/products.json`

### Ejemplo de resultado
![](./sample.png)

## Recursos
- [Documentación oficial de Reactjs](https://reactjs.org/docs)
- [Documentación oficial de Reduxjs](https://redux.js.org/)
- [Repositorio oficial de Create React App](https://github.com/facebook/create-react-app)

***
© [DesafioLatam](https://desafiolatam.com) - Todos los derechos reservados
